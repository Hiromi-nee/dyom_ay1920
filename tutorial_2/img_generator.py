import tensorflow as tf
from tensorflow.keras.preprocessing.image import load_img, img_to_array


# tf.keras.preprocessing.image.load_img(
#     path,
#     grayscale=False,
#     color_mode='rgb',
#     target_size=None,
#     interpolation='nearest'
# )


TRAIN_LIST_FILE = "train.txt"
# list format
# file_path class


def augimg(img, seed=1233):
    pass

def read_imgs(paths, rescale=1./255, target_size=(256,256), augmentation_fn=None):
    # takes in an augmentation function that accepts a
    # an image and returns an augmented image
    imgs = list()
    for p in paths:
        imgs.append(img_to_array(load_img(p, target_size))*rescale)
    return imgs

def img_generator(list_path, num_classes, batch_size=32, target_size=(256,256)):
    print('generator initiated')
    idx = 0

    x_train = list()
    y_train = list()
    with open(list_path, 'r') as f:
        for i, line in enumerate(f):
            x, y = line.rstrip().split(" ")
            x_train.append(x)
            y_train.append(y)
    #print(img_paths)

    while True:
        pad = idx*batch_size
        print('generator yielded a batch %d' % idx)

        # fill in the blanks here
        # Loadd batch of images

        x_train_imgs = read_imgs(x_train[pad:(pad)+batch_size])

        # perform random augmentation
        # one hot encode labels
        # tf.keras.utils.to_categorical
        labels = tf.keras.utils.to_categorical(y_train[pad:(pad)+batch_size], num_classes=num_classes)

        yield x_train[pad:(pad)+batch_size], y_train[pad:(pad)+batch_size]

        idx += 1



def main():
    i_gen = img_generator('sample.txt',
        num_classes=20, batch_size= 2)
    for i in range(1):
        z = next(i_gen)
        print(z[0])


if __name__ == '__main__':
    main()
